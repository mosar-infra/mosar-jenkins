# root/variables.tf

variable "environment" {
  default = "test"
}

variable "managed_by" {
  default = "jenkins"
}

variable "predefined_secrets" {}
