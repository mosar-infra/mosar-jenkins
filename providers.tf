terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "inquisi-dev-div"

    workspaces {
      name = "mosar-jenkins"
    }
  }

  required_version = "~> 1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}
