# ws jenkins /locals.tf

locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
    public  = [for s in data.aws_subnet.public : s.id]
  }
}
locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
    public  = [for s in data.aws_subnet.public : s.cidr_block]
  }
}

locals {
  security_groups = {
    jenkins_lb = {
      name        = "jenkins_lb_sg"
      vpc_id      = data.aws_vpc.mosar_test.id
      description = "security group for jenkins lb"
      ingress_sg  = {}
      ingress_cidr = {
        https = {
          from        = 443
          to          = 443
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
      egress = {
        http = {
          from        = 8080
          to          = 8080
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
    jenkins_internal = {
      name        = "jenkins_internal_sg"
      vpc_id      = data.aws_vpc.mosar_test.id
      description = "security group for internal private access"
      ingress_sg  = {}
      ingress_cidr = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = local.cidr_blocks.private
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = local.cidr_blocks.private
        }
      }
    }
  }
}

locals {
  separate_security_groups = {
    jenkins_private = {
      name         = "private_jenkins_sg"
      vpc_id       = data.aws_vpc.mosar_test.id
      description  = "security group for private access to jenkins"
      ingress_cidr = {}
      ingress_sg = {
        http = {
          from            = 8080
          to              = 8080
          protocol        = "tcp"
          security_groups = module.security_groups.security_groups["jenkins_lb"].*.id
        }
        ssh = {
          from            = 22
          to              = 22
          protocol        = "tcp"
          security_groups = [data.aws_security_group.bastion_public.id]
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}

locals {
  roles = {
    jenkins = {
      role = {
        name               = "jenkins"
        description        = "role for jenkins to access s3 bucket and secrets"
        assume_role_policy = file("${path.root}/iam/ec2_assume_role_policy.json")
      }
      policy = {
        name        = "jenkins_policy"
        description = "Policy to allow jenkins to access s3 and secrets"
        policy      = file("${path.root}/iam/jenkins_role_policy.json")
      }
      policy_attachment = {
        name = "jenkins-policy-attachment"
      }
    }
  }
}

locals {
  profiles = {
    jenkins = {
      name = "jenkins_profile"
    }
  }
}

locals {
  nodes = {
    jenkins = {
      instance_type               = "t3.medium"
      subnet_ids                  = local.subnets.private
      vpc_security_group_ids      = concat(module.security_groups.separate_security_groups["jenkins_private"].*.id, module.security_groups.security_groups["jenkins_internal"].*.id)
      associate_public_ip_address = false
      root_block_device_vol_size  = 30
      iam_instance_profile        = module.iam.profile["jenkins"].name
      tags_name_prefix            = "jenkins_server_"
      user_data_file_path         = "./user_data_scripts/setup_jenkins.tpl"
      script_vars                 = local.script_vars
      private_ip                  = null
      key_name                    = data.aws_key_pair.mosar_server_ssh1.key_name
    }
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}
locals {
  public_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*public*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}

locals {
  buckets = {
    jenkins_configs = {
      version        = "version_1" # just for keepers
      bucket         = "devdiv-jenkins"
      acl            = "private"
      file_directory = "${path.root}/buckets/devdiv-jenkins"
    }
  }
}

locals {
  secret_names = {
    gitlab-api-token      = {}
    jenkins-credentials   = {}
    dockerhub-credentials = {}
    gitlab-credentials    = {}
    jenkins-url           = {}
  }
}

locals {
  secrets = { for k, v in local.secret_names : k => data.aws_secretsmanager_secret_version.by_id[k].secret_string }
}

locals {
  script_vars = merge(local.secrets, {bucket = module.s3["jenkins_configs"].bucket.bucket})
}

locals {
  loadbalancers = {
    mosar_jenkins_lb = {
      lb_name                   = "mosar-jenkins-lb"
      public_subnet_ids         = local.subnets.public
      public_security_group_ids = [module.security_groups.security_groups["jenkins_lb"].id]
      vpc_id                    = data.aws_vpc.mosar_test.id
      listener_port             = 443
      listener_protocol         = "HTTPS"
      ssl_policy                = "ELBSecurityPolicy-2016-08"
      certificate_arn           = data.aws_acm_certificate.mosar_jenkins.arn
      lb_tg_name                = "jenkins-lb-tg"
      lb_tg_target_type         = "instance"
      lb_tg_target_id           = module.ec2.aws_instance["jenkins"].id
      lb_tg_port                = 8080
      lb_tg_protocol            = "HTTP"
      lb_tg_path                = "/login"
      lb_tg_healthy_threshold   = 3
      lb_tg_unhealthy_threshold = 3
      lb_tg_timeout             = 3
      lb_tg_interval            = 15
      add_attachment            = true
    }
  }
}

