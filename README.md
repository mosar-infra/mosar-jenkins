# Mosar Jenkins

This defines the workspace managing the resources for the jekins server.
It depends on external secrets, manages in AWS Secrets Manager for:

- gitlab-api-token
- jenkins-credentials
- dockerhub-credentials
- gitlab-credentials

It will create the server, security_groups, an IAM role to run the setup and an s3 bucket with the files in it necessary to deploy Jenkins in a container.


