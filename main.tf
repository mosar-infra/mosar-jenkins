# ws jenkins /main.tf

module "iam" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v3.0.2"
  roles       = local.roles
  profiles    = local.profiles
  environment = "test"
}

resource "random_id" "force_replace" {
  byte_length = 8
  keepers     = merge(
    local.secrets,
    {
      user_data = file(local.nodes["jenkins"].user_data_file_path)
    }
  )
}

resource "random_string" "string" {
  length  = 4
  special = false
  upper   = false
  keepers = {
    name = local.buckets["jenkins_configs"].version
  }
}

module "ec2" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-ec2.git?ref=tags/v1.0.3"
  ami_name_string = "amzn2-ami-hvm-2.0*"
  ami_owners      = ["amazon"]
  nodes           = local.nodes
  environment     = var.environment
  managed_by      = "jenkins"
  depends_on = [
    random_id.force_replace
  ]
}

module "security_groups" {
  source                   = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.1"
  environment              = var.environment
  managed_by               = var.managed_by
  security_groups          = local.security_groups
  separate_security_groups = local.separate_security_groups
}

module "s3" {
  source         = "git::https://gitlab.com/mosar-infra/tf-module-s3.git?ref=tags/v1.0.9"
  for_each       = local.buckets
  bucket         = "${each.value.bucket}-${random_string.string.result}"
  acl            = each.value.acl
  file_directory = each.value.file_directory
  environment    = var.environment
  managed_by     = var.managed_by
}

module "loadbalancer" {
  source                    = "git::https://gitlab.com/mosar-infra/tf-module-loadbalancer.git?ref=tags/v1.0.6"
  for_each                  = local.loadbalancers
  lb_name                   = each.value.lb_name
  public_subnet_ids         = each.value.public_subnet_ids
  public_security_group_ids = each.value.public_security_group_ids
  vpc_id                    = each.value.vpc_id
  listener_port             = each.value.listener_port
  listener_protocol         = each.value.listener_protocol
  ssl_policy                = each.value.ssl_policy
  certificate_arn           = each.value.certificate_arn
  lb_tg_name                = each.value.lb_tg_name
  lb_tg_port                = each.value.lb_tg_port
  lb_tg_protocol            = each.value.lb_tg_protocol
  lb_tg_target_type         = each.value.lb_tg_target_type
  lb_tg_target_id           = each.value.lb_tg_target_id
  lb_tg_healthy_threshold   = each.value.lb_tg_healthy_threshold
  lb_tg_unhealthy_threshold = each.value.lb_tg_unhealthy_threshold
  lb_tg_path                = each.value.lb_tg_path
  lb_tg_timeout             = each.value.lb_tg_timeout
  lb_tg_interval            = each.value.lb_tg_interval
  add_attachment            = each.value.add_attachment
  environment               = var.environment
  managed_by                = var.managed_by
}

module "secrets" {
  source                   = "git::https://gitlab.com/mosar-infra/tf-module-secrets.git?ref=tags/v3.1.2"
  predefined_secrets       = var.predefined_secrets
  environment              = var.environment
  managed_by               = var.managed_by

}
