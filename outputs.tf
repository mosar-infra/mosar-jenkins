# ws jenkins / output
output "iam" {
  value = module.iam
}

output "ec2" {
  value     = module.ec2
  sensitive = true
}

output "s3" {
  value = module.s3
}

output "loadbalancer" {
  value = module.loadbalancer
}
