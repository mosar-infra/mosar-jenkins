# ws jenkins /datasource

data "aws_vpc" "mosar_test" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnets" "private" {
  dynamic "filter" {
    for_each = local.private_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}

data "aws_subnets" "public" {
  dynamic "filter" {
    for_each = local.public_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "public" {
  for_each = toset(data.aws_subnets.public.ids)
  id       = each.value
}

data "aws_key_pair" "mosar_server_ssh1" {
  key_name = "mosar_server_ssh1"
  filter {
    name   = "tag:Environment"
    values = ["test"]
  }
}

data "aws_security_group" "bastion_public" {
  name = "bastion_public_sg"
  tags = {
    Environment = "test"
  }
}

data "aws_secretsmanager_secret" "by_name" {
  for_each = local.secret_names
  name     = each.key
}

data "aws_secretsmanager_secret_version" "by_id" {
  for_each  = local.secret_names
  secret_id = data.aws_secretsmanager_secret.by_name[each.key].id
}

data "aws_acm_certificate" "mosar_jenkins" {
  domain = "jenkins.inquisitive.nl"
}

