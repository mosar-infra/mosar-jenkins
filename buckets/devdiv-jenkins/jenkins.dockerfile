FROM jenkins/jenkins:lts-jdk11


# tricks looked at:
# https://www.popularowl.com/jenkins/automating-jenkins-install-docker-terraform/
# Jenkins runs all grovy files from init.groovy.d dir
# use this for creating default admin user, and pointing to the correct external URL. (only does not work locally?)
COPY initial-config.groovy /usr/share/jenkins/ref/init.groovy.d/

COPY jenkins.yaml /usr/share/jenkins/ref/jenkins.yaml
COPY jenkins.yaml /config-backup/jenkins.yaml
COPY ./jenkins-plugins /usr/share/jenkins/plugins
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/plugins

ENV JENKINS_USER jenkins-user
ENV JENKINS_PASSWORD jenkins-password
ENV GITLAB_USER gitlab-user
ENV GITLAB_PASSWORD gitlab-password
ENV GITLAB_API_TOKEN gitlab-token
ENV DOCKERHUB_USER dockerhub-user
ENV DOCKERHUB_PASSWORD dockerhub-password
ENV JENKINS_URL jeninks-url
ENV EC2_SSH prv-key

ENV GID_DOCKER 992
# this is the GID for docker group on ec2.

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
ENV JAVA_OPTS -Dcasc.jenkins.config=/config-backup
# used trick i for setting the correct rights on volume is: https://github.com/carlossg/jenkins-for-volumes-docker

USER root

RUN curl -sSL https://get.docker.com/ | sh && \
    groupmod -g $GID_DOCKER docker && \
    usermod -a -G docker jenkins


COPY start.sh /usr/local/bin/start.sh
# gosu
RUN apt-get update && apt-get install -y --no-install-recommends gosu && \
  chmod +x /usr/local/bin/start.sh

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/start.sh"]
